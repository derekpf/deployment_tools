#!/usr/bin/env python3
#----------------------------------------------------------------------------------------------------------------------
# Author: Derek Lee
# Overview: QR Code creation tool for brother printers. Creates QR code png / label.xml / prop.xml in a new folder
#          and zips them for easy printing
#----------------------------------------------------------------------------------------------------------------------
from zipfile import ZipFile
import pprint
import qrcode
import argparse
import requests as requests
import os
import shutil 
import re
from PIL import Image

from pfutils import flask as flaskutil


argp = argparse.ArgumentParser(usage = 'Two modes: -t or -f. -t uses tree... Only use this if you know ACC tree ' +
        'exists')
argp.add_argument('--treemode', '-t', action = 'store_true', help = 'tree mode! Uses flask')
argp.add_argument('--filemode', '-f', action = 'store_true', help = 'using file mode!')
argp.add_argument('--runfilemode', '-fr', action = 'store_true', help = 'run qr creation with a file')
argp.add_argument('--single', '-s', action = 'store_true', help = 'using single mode')

args = vars(argp.parse_args())
#----------------------------------------------------------------------------------------------------------------------
# Parameters: None
# Overview: Uses flask to retrieve acg/acs and parking space information. Then uses it to make QR code/space stickers.
#----------------------------------------------------------------------------------------------------------------------
def create_qr():
    pp = pprint.PrettyPrinter(indent=4)
    
    acn_id = input("ACN? : ").zfill(4)
    acc_id = input("ACC? : ").zfill(2)
    path = os.getcwd()

    #flaskutil.change_FLASK_URL('https://flask-dev.powerflexsystems.com')
    
    print("Retrieving tree")
    try:
        acc_tree = flaskutil.get_acc_tree(acn_id, acc_id)
    except requests.exceptions.HTTPError:
        print("Failed to retrieve tree...")
    try:
        os.makedirs(path + '/qrcodes')
        os.makedirs(path + '/images')
        os.makedirs(path + '/lbx1')
    except OSError:
        print("Failed to create directory. Does it already exist?")

    pp.pprint(acc_tree['acc'])
    for acg in acc_tree['acc']:
        for acs in acc_tree['acc'][acg]:
            if acs.isdigit():
                qr = qrcode.QRCode(
                    version = 1,
                    error_correction = qrcode.constants.ERROR_CORRECT_H,
                    box_size = 10,
                    border = 4,
                )
                pfid = acn_id + acc_id + acg + acs
                qr.add_data(pfid)
                qr.make(fit=True)
                try:
                    os.makedirs(path + "/qrcodes/" + pfid + "/")
                except OSError:
                    print("Failed to make inner pfid directory")
                shutil.copy2(path + "/prop.xml", path + "/qrcodes/" + pfid) 
                qr.make_image().save(path+"/images/{0}.jpg".format(pfid))
                im = Image.open(path + '/images/{0}.jpg'.format(pfid))
                im.save(path + '/qrcodes/' + pfid + '/Object0.bmp')
                #---- begin sed for label.xml ----
                try:
                    with open(path + '/label.xml') as f:
                        for line in f:
                            #res = re.sub(r' 5678 ', acc_tree['acc'][acg][acs]['parking_space'], str(line))
                            res = re.sub(r' 5678 ', acs, str(line))
                            res = re.sub(r' 1234 ', acc_tree['acc_name'][:4] + acc_tree['acc'][acg][acs]['parking_space'][3:5], res)
                            #res = re.sub(r' 1234 ', acc_tree['acc_name'], res)
                            try:
                                fw = open(path + '/qrcodes/' + pfid + '/label.xml', 'w+')
                                fw.write(res)
                                fw.close()
                            except IOError:
                                print("failed to open qrcodes/label.xml")
                            if os.getcwd() != path:
                                pass
                            else:
                                os.chdir(path + '/lbx1')
                            shutil.make_archive(pfid, 'zip', path + '/qrcodes/' + pfid + '/')
                            zip2lbx = path + '/lbx1/' + pfid + '.zip'
                            base = os.path.splitext(zip2lbx)[0]
                            os.rename(zip2lbx, base + '.lbx')
                        f.close()
                
                except IOError:
                    print("Failed. Could have been a lot of different things.")

def create_single():
    path = os.getcwd()

    site_name = input("Site Name/Top Block of Sticker?: ")
    num_stations = int(input("Number of stations?: "))
    pid = input("ACN + ACC + ACG?: ")

    try:
        os.makedirs(path + '/singleqrcodes')
        os.makedirs(path + '/singleimages')
        os.makedirs(path + '/singlelbx')
    except OSError:
        print("Failed to create directory. Does it already exist?")
    for i in range(num_stations):
        qr = qrcode.QRCode(
                    version = 1,
                    error_correction = qrcode.constants.ERROR_CORRECT_H,
                    box_size = 10,
                    border = 4,
                )
        pfid = pid + str(i+1).zfill(2)
        qr.add_data(pfid)
        qr.make(fit=True)
        try:
            os.makedirs(path + "/singleqrcodes/" + pfid + "/")
        except OSError:
            print("failed to make inner pfid directory")
        shutil.copy2(path + "/prop.xml", path + "/singleqrcodes/" + pfid)
        qr.make_image().save(path+"/singleimages/{0}.jpg".format(pfid))
        im = Image.open(path + '/singleimages/{0}.jpg'.format(pfid))
        im.save(path + '/singleqrcodes/' + pfid + '/Object0.bmp')

        try:
            with open(path + '/label.xml') as f:
                for line in f:
                    res = re.sub(r' 5678 ', str(i+1).zfill(2), str(line))
                    res = re.sub(r' 1234 ', site_name , res)
                    try:
                        fw = open(path + '/singleqrcodes/' + pfid + '/label.xml', 'w+')
                        fw.write(res)
                        fw.close()
                    except IOError:
                        print("failed to open qrcodes/label.xml")
                    if os.getcwd() != path:
                        pass
                    else:
                        os.chdir(path + '/singlelbx')
                    shutil.make_archive(pfid, 'zip', path + '/singleqrcodes/' + pfid + '/')
                    zip2lbx = path + '/singlelbx/' + pfid + '.zip'
                    base = os.path.splitext(zip2lbx)[0]
                    os.rename(zip2lbx, base + '.lbx')
                f.close()
        except IOError:
            print("Failed. Could have been a lot of different things.")

def create_file():
    print("Warning!!! This QR code creation mode only supports FLAT trees(1 acg)")
    f = open("config_qr.txt", "w+")
    num_stations = int(input("How many stations are there?: "))
    acn_id = input("ACN?: ").zfill(4)
    acc_id = input("ACC?: ").zfill(2)
    
    for i in range(num_stations):
        i += 1
        f.write('pfid: {0}\n'.format(acn_id + acc_id + '01' + str(i).zfill(2)))
        f.write('parking space: {0}\n'.format(str(i).zfill(2)))
    f.close()
    print("config_qr.txt created please edit if you want to make changes")

def create_qr_file():
    pp = pprint.PrettyPrinter(indent=4)
    print("NOTE: THESE QR CODES WONT WORK UNTIL FLASK HAS BEEN UDPATED") 
    path = os.getcwd()
    try:
        os.makedirs(path + '/qrcodes')
        os.makedirs(path + '/images')
        os.makedirs(path + '/lbx')
    except:
        print("Failed to create directory. Does it already exist?")
    loop_itr = 0
    try:
        with open('config_qr.txt') as f:
            for line in f:
                if loop_itr == 0:
                    pfid = line.replace('pfid: ', '').rstrip()
                elif loop_itr == 1:
                    parking_space = line.replace('parking space: ', '').rstrip()
                loop_itr += 1
                if loop_itr == 2:
                    #begin creating qr codes
                    qr = qrcode.QRCode(
                        version = 1,
                        error_correction = qrcode.constants.ERROR_CORRECT_H,
                        box_size = 10,
                        border = 4,
                    )
                    qr.add_data(pfid)
                    qr.make(fit=True)
                    try:
                        os.makedirs(path + "/qrcodes/" + pfid + "/")
                    except OSError:
                        print("Failed to make inner pfid directory")
                    shutil.copy2(path + "/prop.xml", path + "/qrcodes/" + pfid)
                    qr.make_image().save(path+"/images/{0}.jpg".format(pfid))
                    im = Image.open(path + '/images/{0}.jpg'.format(pfid))
                    im.save(path + '/qrcodes/' + pfid + '/Object0.bmp')
                    #---- begin sed for label.xml ----
                    try:
                        with open(path + '/label.xml') as f:
                            space = re.compile(r' 1234 ' )
                            for line in f:
                                test = space.search(line)
                                edited = space.sub(parking_space, str(line))
                                try:
                                    fw = open(path + '/qrcodes/' + pfid + '/label.xml', 'w+')
                                    fw.write(edited)
                                    fw.close()
                                except IOError:
                                    print("failed to open qrcodes/label.xml")
                                if os.getcwd() != path:
                                    pass
                                else:
                                    os.chdir(path + '/lbx')
                                shutil.make_archive(pfid, 'zip', path + '/qrcodes/' + pfid + '/')
                                zip2lbx = path + '/lbx/' + pfid + '.zip'
                                base = os.path.splitext(zip2lbx)[0]
                                os.rename(zip2lbx, base + '.lbx')
                            f.close()

                    except IOError:
                        print("Failed. Could have been a lot of different things.")
                    loop_itr = 0
    except IOError:
        print("Could not open and read file")

if args['single'] == True:
    create_single()
if args['treemode'] == True:
    create_qr()
if args['filemode'] == True:
    create_file()
if args['runfilemode'] == True:
    create_qr_file()
